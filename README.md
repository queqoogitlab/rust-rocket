# 🦀 Rust App with Rocket and Diesel 🦀

This project i have copied and edited from [this](https://blog.logrocket.com/create-web-app-rust-rocket-diesel/) 😅

## Local Setup Postgres
* docker run --name some-postgres -p <exposed_port>:5432 -e POSTGRES_PASSWORD=1234 -d postgres:latest

## Postgres Script for initialization 
* psql -U postgres -w
* create database blog;

## Rust Party
* (Just install it for diesel_cli) brew install postgresql
* cargo install diesel_cli --no-default-features --features postgres
* create .env with DATABASE_URL=postgres://username:password@localhost:<exposed_port>/blog

* diesel setup
* diesel migration generate posts
* edit: down.sql and up.sql
* diesel migration run


## (Optional) Error: Pear requires a 'dev' or 'nightly' version of rustc.
* rustup install nightly
* rustup override set nightly

## Todo 🐳
* containerize

