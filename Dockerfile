FROM rust:1.70.0

ENV DATABASE_URL=postgres://postgres:1234@localhost:5432/blog
# ENV DATABASE_URL=postgres://postgres:1234@host.docker.internal:5432/blog

WORKDIR /app

# # Install system dependencies
# RUN apt-get update && apt-get install -y libpq-dev

COPY . .

RUN rustup default nightly
RUN cargo build

EXPOSE 8000

CMD ["cargo", "run"]


